﻿using FinalYearProjectManagementApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalYearProjectManagementApp
{
    public partial class ManageProjects : Form
    {
        private int selectedProjectID = 0;
        public ManageProjects()
        {
            InitializeComponent();
            PopulateDataGridView();

            projectdatagridView.CellClick += projectdatagridView_CellClick;
        }

        private void PopulateDataGridView()
        {
            // SQL query to select data from the Project table
            string query = @"SELECT Id AS ProjectID, Title, Description FROM Project";

            try
            {
                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Projects");
                projectdatagridView.DataSource = dataSet.Tables["Projects"];
                projectdatagridView.AutoResizeColumns();
                projectdatagridView.Columns["ProjectID"].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void projectdatagridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) // Check if a valid row is clicked
            {
                DataGridViewRow row = projectdatagridView.Rows[e.RowIndex];

                // Populate textboxes or controls with data from the selected row
                txt_title.Text = row.Cells["Title"].Value.ToString();
                txt_description.Text = row.Cells["Description"].Value.ToString();
                // Retrieve and store the PersonID from the clicked row
                selectedProjectID = Convert.ToInt32(row.Cells["ProjectID"].Value);
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            // Pass the updated data to the update method
            UpdateProjectData(selectedProjectID, txt_title.Text, txt_description.Text);

            PopulateDataGridView();
            ClearProjectData();
        }

        private void UpdateProjectData(int selectedProjectID, string title, string description)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                string updateQuery = @"UPDATE Project
                               SET Title = @Title, Description = @Description WHERE Id = @ProjectId";
                SqlCommand projectCommand = new SqlCommand(updateQuery, connection);
                projectCommand.Parameters.AddWithValue("@Title", title);
                projectCommand.Parameters.AddWithValue("@Description", description);
                projectCommand.Parameters.AddWithValue("@ProjectId", selectedProjectID);
                projectCommand.ExecuteNonQuery();
                MessageBox.Show("Data updated successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            // Ensure that a row is selected
            if (projectdatagridView.SelectedRows.Count > 0)
            {
                // Get the ID of the selected project
                int projectId = Convert.ToInt32(projectdatagridView.SelectedRows[0].Cells["ProjectID"].Value);

                // Delete the person record from the project table
                DeletePerson(projectId);

                // Refresh DataGridView to reflect the changes
                PopulateDataGridView();

                // Clear the textboxes or controls after deletion
                ClearProjectData();
            }
            else
            {
                MessageBox.Show("Please select a row to delete.");
            }
        }

        private void DeletePerson(int projectId)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                // Delete the corresponding record from the Student table
                string deleteQuery = @"DELETE FROM Project WHERE Id = @ProjectId";
                SqlCommand projectCommand = new SqlCommand(deleteQuery, connection);
                projectCommand.Parameters.AddWithValue("@ProjectId", projectId);
                projectCommand.ExecuteNonQuery();

                MessageBox.Show("Record deleted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            InsertProjectData(txt_title.Text, txt_description.Text);

            PopulateDataGridView();
            ClearProjectData();
        }

        private void InsertProjectData(string title, string description)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                string personquery = @"INSERT INTO Project (Title, Description) 
                    VALUES (@Title, @Description);";
                SqlCommand command = new SqlCommand(personquery, connection);
                command.Parameters.AddWithValue("@Title", title);
                command.Parameters.AddWithValue("@Description", description);
                command.ExecuteScalar();

                MessageBox.Show("Data inserted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }


        private void ClearProjectData()
        {
            txt_title.Text = "";
            txt_description.Text = "";
            this.selectedProjectID = 0;
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Hide();
        }

        private void btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
