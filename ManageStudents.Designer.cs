﻿namespace FinalYearProjectManagementApp
{
    partial class ManageStudents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_firstname = new System.Windows.Forms.TextBox();
            this.lbl_firstname = new System.Windows.Forms.Label();
            this.txt_lastname = new System.Windows.Forms.TextBox();
            this.lbl_lastname = new System.Windows.Forms.Label();
            this.txt_contact = new System.Windows.Forms.TextBox();
            this.lbl_contact = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.studentdatagridView = new System.Windows.Forms.DataGridView();
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.lbl_gender = new System.Windows.Forms.Label();
            this.dateofbirth = new System.Windows.Forms.DateTimePicker();
            this.lbl_dob = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.txt_RegistrationNo = new System.Windows.Forms.TextBox();
            this.lbl_RegistrationNo = new System.Windows.Forms.Label();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btn_Back = new System.Windows.Forms.Button();
            this.btn_Exist = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.studentdatagridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(587, 61);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Manage Students";
            // 
            // txt_firstname
            // 
            this.txt_firstname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_firstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_firstname.Location = new System.Drawing.Point(60, 214);
            this.txt_firstname.Margin = new System.Windows.Forms.Padding(6);
            this.txt_firstname.Name = "txt_firstname";
            this.txt_firstname.Size = new System.Drawing.Size(365, 37);
            this.txt_firstname.TabIndex = 1;
            // 
            // lbl_firstname
            // 
            this.lbl_firstname.AutoSize = true;
            this.lbl_firstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstname.Location = new System.Drawing.Point(55, 159);
            this.lbl_firstname.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_firstname.Name = "lbl_firstname";
            this.lbl_firstname.Size = new System.Drawing.Size(152, 30);
            this.lbl_firstname.TabIndex = 2;
            this.lbl_firstname.Text = "First name *";
            // 
            // txt_lastname
            // 
            this.txt_lastname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_lastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lastname.Location = new System.Drawing.Point(526, 214);
            this.txt_lastname.Margin = new System.Windows.Forms.Padding(6);
            this.txt_lastname.Name = "txt_lastname";
            this.txt_lastname.Size = new System.Drawing.Size(365, 37);
            this.txt_lastname.TabIndex = 3;
            // 
            // lbl_lastname
            // 
            this.lbl_lastname.AutoSize = true;
            this.lbl_lastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lastname.Location = new System.Drawing.Point(521, 159);
            this.lbl_lastname.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_lastname.Name = "lbl_lastname";
            this.lbl_lastname.Size = new System.Drawing.Size(132, 30);
            this.lbl_lastname.TabIndex = 4;
            this.lbl_lastname.Text = "Last name";
            // 
            // txt_contact
            // 
            this.txt_contact.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_contact.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contact.Location = new System.Drawing.Point(977, 214);
            this.txt_contact.Margin = new System.Windows.Forms.Padding(6);
            this.txt_contact.Name = "txt_contact";
            this.txt_contact.Size = new System.Drawing.Size(365, 37);
            this.txt_contact.TabIndex = 5;
            // 
            // lbl_contact
            // 
            this.lbl_contact.AutoSize = true;
            this.lbl_contact.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contact.Location = new System.Drawing.Point(972, 159);
            this.lbl_contact.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_contact.Name = "lbl_contact";
            this.lbl_contact.Size = new System.Drawing.Size(101, 30);
            this.lbl_contact.TabIndex = 6;
            this.lbl_contact.Text = "Contact";
            // 
            // txt_email
            // 
            this.txt_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(60, 336);
            this.txt_email.Margin = new System.Windows.Forms.Padding(6);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(365, 37);
            this.txt_email.TabIndex = 7;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(55, 290);
            this.lbl_email.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(95, 30);
            this.lbl_email.TabIndex = 8;
            this.lbl_email.Text = "Email *";
            // 
            // studentdatagridView
            // 
            this.studentdatagridView.AllowUserToAddRows = false;
            this.studentdatagridView.AllowUserToOrderColumns = true;
            this.studentdatagridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentdatagridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.studentdatagridView.Location = new System.Drawing.Point(60, 522);
            this.studentdatagridView.Margin = new System.Windows.Forms.Padding(6);
            this.studentdatagridView.Name = "studentdatagridView";
            this.studentdatagridView.RowHeadersWidth = 72;
            this.studentdatagridView.Size = new System.Drawing.Size(1282, 392);
            this.studentdatagridView.TabIndex = 9;
            // 
            // genderComboBox
            // 
            this.genderComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Location = new System.Drawing.Point(977, 336);
            this.genderComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(363, 38);
            this.genderComboBox.TabIndex = 10;
            // 
            // lbl_gender
            // 
            this.lbl_gender.AutoSize = true;
            this.lbl_gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gender.Location = new System.Drawing.Point(972, 290);
            this.lbl_gender.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_gender.Name = "lbl_gender";
            this.lbl_gender.Size = new System.Drawing.Size(98, 30);
            this.lbl_gender.TabIndex = 11;
            this.lbl_gender.Text = "Gender";
            // 
            // dateofbirth
            // 
            this.dateofbirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateofbirth.Location = new System.Drawing.Point(526, 340);
            this.dateofbirth.Margin = new System.Windows.Forms.Padding(6);
            this.dateofbirth.Name = "dateofbirth";
            this.dateofbirth.Size = new System.Drawing.Size(363, 37);
            this.dateofbirth.TabIndex = 12;
            // 
            // lbl_dob
            // 
            this.lbl_dob.AutoSize = true;
            this.lbl_dob.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dob.Location = new System.Drawing.Point(521, 290);
            this.lbl_dob.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_dob.Name = "lbl_dob";
            this.lbl_dob.Size = new System.Drawing.Size(152, 30);
            this.lbl_dob.TabIndex = 13;
            this.lbl_dob.Text = "Date of birth";
            // 
            // btn_Save
            // 
            this.btn_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.Location = new System.Drawing.Point(842, 945);
            this.btn_Save.Margin = new System.Windows.Forms.Padding(6);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(138, 59);
            this.btn_Save.TabIndex = 14;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Update
            // 
            this.btn_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Update.Location = new System.Drawing.Point(990, 945);
            this.btn_Update.Margin = new System.Windows.Forms.Padding(6);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(138, 59);
            this.btn_Update.TabIndex = 15;
            this.btn_Update.Text = "Update";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Delete.Location = new System.Drawing.Point(1139, 945);
            this.btn_Delete.Margin = new System.Windows.Forms.Padding(6);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(203, 59);
            this.btn_Delete.TabIndex = 16;
            this.btn_Delete.Text = "Delete";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // txt_RegistrationNo
            // 
            this.txt_RegistrationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_RegistrationNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RegistrationNo.Location = new System.Drawing.Point(60, 462);
            this.txt_RegistrationNo.Margin = new System.Windows.Forms.Padding(6);
            this.txt_RegistrationNo.Name = "txt_RegistrationNo";
            this.txt_RegistrationNo.Size = new System.Drawing.Size(365, 37);
            this.txt_RegistrationNo.TabIndex = 17;
            // 
            // lbl_RegistrationNo
            // 
            this.lbl_RegistrationNo.AutoSize = true;
            this.lbl_RegistrationNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_RegistrationNo.Location = new System.Drawing.Point(55, 415);
            this.lbl_RegistrationNo.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_RegistrationNo.Name = "lbl_RegistrationNo";
            this.lbl_RegistrationNo.Size = new System.Drawing.Size(207, 30);
            this.lbl_RegistrationNo.TabIndex = 18;
            this.lbl_RegistrationNo.Text = "Registration No *";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btn_Back
            // 
            this.btn_Back.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Back.Location = new System.Drawing.Point(60, 945);
            this.btn_Back.Margin = new System.Windows.Forms.Padding(6);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(138, 59);
            this.btn_Back.TabIndex = 19;
            this.btn_Back.Text = "Back";
            this.btn_Back.UseVisualStyleBackColor = true;
            this.btn_Back.Click += new System.EventHandler(this.btn_Back_Click);
            // 
            // btn_Exist
            // 
            this.btn_Exist.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exist.Location = new System.Drawing.Point(210, 945);
            this.btn_Exist.Margin = new System.Windows.Forms.Padding(6);
            this.btn_Exist.Name = "btn_Exist";
            this.btn_Exist.Size = new System.Drawing.Size(138, 59);
            this.btn_Exist.TabIndex = 20;
            this.btn_Exist.Text = "Exit";
            this.btn_Exist.UseVisualStyleBackColor = true;
            this.btn_Exist.Click += new System.EventHandler(this.btn_Exist_Click);
            // 
            // ManageStudents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1409, 1044);
            this.Controls.Add(this.btn_Exist);
            this.Controls.Add(this.btn_Back);
            this.Controls.Add(this.lbl_RegistrationNo);
            this.Controls.Add(this.txt_RegistrationNo);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.lbl_dob);
            this.Controls.Add(this.dateofbirth);
            this.Controls.Add(this.lbl_gender);
            this.Controls.Add(this.genderComboBox);
            this.Controls.Add(this.studentdatagridView);
            this.Controls.Add(this.lbl_email);
            this.Controls.Add(this.txt_email);
            this.Controls.Add(this.lbl_contact);
            this.Controls.Add(this.txt_contact);
            this.Controls.Add(this.lbl_lastname);
            this.Controls.Add(this.txt_lastname);
            this.Controls.Add(this.lbl_firstname);
            this.Controls.Add(this.txt_firstname);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "ManageStudents";
            this.Text = "Manage Students";
            ((System.ComponentModel.ISupportInitialize)(this.studentdatagridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_firstname;
        private System.Windows.Forms.Label lbl_firstname;
        private System.Windows.Forms.TextBox txt_lastname;
        private System.Windows.Forms.Label lbl_lastname;
        private System.Windows.Forms.TextBox txt_contact;
        private System.Windows.Forms.Label lbl_contact;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.DataGridView studentdatagridView;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.Label lbl_gender;
        private System.Windows.Forms.DateTimePicker dateofbirth;
        private System.Windows.Forms.Label lbl_dob;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.TextBox txt_RegistrationNo;
        private System.Windows.Forms.Label lbl_RegistrationNo;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btn_Back;
        private System.Windows.Forms.Button btn_Exist;
    }
}