﻿using FinalYearProjectManagementApp.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace FinalYearProjectManagementApp
{
    public partial class ManageStudents : Form
    {
        private Dictionary<int, string> genderDictionary = new Dictionary<int, string>();
        private int selectedPersonID;
        public ManageStudents()
        {
            InitializeComponent();
            PopulateGenderComboBox();
            PopulateDataGridView();

            studentdatagridView.CellClick += studentdatagridView_CellClick;
        }

        private void PopulateDataGridView()
        {
            // SQL query to select data from the Students table
            string query = @"SELECT P.Id AS PersonID, P.FirstName, P.LastName, P.Contact, P.Email, P.DateOfBirth, p.Gender AS GenderID, L.Value AS Gender,
                    S.RegistrationNo, S.Id AS StudentID
                    FROM Person P 
                    INNER JOIN Student S ON P.Id = S.Id
                    LEFT JOIN LookUp L on P.Gender = L.Id AND L.Category = 'GENDER'";

            try
            {
                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Students");
                studentdatagridView.DataSource = dataSet.Tables["Students"];
                studentdatagridView.AutoResizeColumns();
                studentdatagridView.Columns["GenderID"].Visible = false;
                studentdatagridView.Columns["StudentID"].Visible = false;
                studentdatagridView.Columns["PersonID"].Visible = false;
                // Set the format for the DateOfBirth column to display only date part
                studentdatagridView.Columns["DateOfBirth"].DefaultCellStyle.Format = "yyyy-MM-dd";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void PopulateGenderComboBox()
        {
            genderComboBox.DataSource = GetGenders().ToList(); // Bind directly to the dictionary
            genderComboBox.DisplayMember = "Value"; // Display the designation string
            genderComboBox.ValueMember = "Key"; // Use the ID as the value

            // Set default selected value
            if (genderComboBox.Items.Count > 0)
            {
                genderComboBox.SelectedIndex = 0;
            }
        }

        private Dictionary<int, string> GetGenders()
        {
            try
            {
                Dictionary<int, string> genders = new Dictionary<int, string>();
                string query = @"SELECT Id, Value AS Gender FROM LookUp WHERE Category = 'GENDER'";

                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);

                int default_id = Convert.ToInt32(0);
                string default_gender = "Select gender";
                genders.Add(default_id, default_gender);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader["Id"]);
                        string gender = reader["Gender"].ToString();
                        genders.Add(id, gender);
                    }
                }
                return genders;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return null;
            }
        }

        private void studentdatagridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) // Check if a valid row is clicked
            {
                DataGridViewRow row = studentdatagridView.Rows[e.RowIndex];

                // Populate textboxes or controls with data from the selected row
                txt_firstname.Text = row.Cells["FirstName"].Value.ToString();
                txt_lastname.Text = row.Cells["LastName"].Value.ToString();
                txt_contact.Text = row.Cells["Contact"].Value.ToString();
                txt_email.Text = row.Cells["Email"].Value.ToString();
                dateofbirth.Value = Convert.ToDateTime(row.Cells["DateOfBirth"].Value);
                genderComboBox.SelectedValue = Convert.ToInt32(row.Cells["GenderID"].Value);
                txt_RegistrationNo.Text = row.Cells["RegistrationNo"].Value.ToString();

                // Retrieve and store the PersonID from the clicked row
                selectedPersonID = Convert.ToInt32(row.Cells["PersonID"].Value);
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            // Create a new instance of PersonModel and populate it with updated data
            PersonModel updatedPerson = new PersonModel();
            updatedPerson.Id = selectedPersonID;
            updatedPerson.FirstName = txt_firstname.Text;
            updatedPerson.LastName = txt_lastname.Text;
            updatedPerson.Contact = txt_contact.Text;
            updatedPerson.Email = txt_email.Text;
            updatedPerson.DateofBirth = dateofbirth.Value;
            if (genderComboBox.SelectedValue != null)
            {
                updatedPerson.GenderId = Convert.ToInt32(genderComboBox.SelectedValue);
            }
            updatedPerson.RegistrationNo = txt_RegistrationNo.Text;

            // Pass the updated data to the update method
            UpdateStudentData(updatedPerson);

            PopulateDataGridView();
            clearStudentData();
        }

        private void UpdateStudentData(PersonModel updatedPerson)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                string updateQuery = @"UPDATE Person
                               SET FirstName = @FirstName, LastName = @LastName, Email = @Email, 
                                   Contact = @Contact, DateOfBirth = @DateOfBirth, Gender = @Gender
                               WHERE Id = @PersonId";

                SqlCommand personCommand = new SqlCommand(updateQuery, connection);
                personCommand.Parameters.AddWithValue("@FirstName", updatedPerson.FirstName);
                personCommand.Parameters.AddWithValue("@LastName", updatedPerson.LastName);
                personCommand.Parameters.AddWithValue("@Email", updatedPerson.Email);
                personCommand.Parameters.AddWithValue("@Contact", updatedPerson.Contact);
                personCommand.Parameters.AddWithValue("@DateOfBirth", updatedPerson.DateofBirth);
                personCommand.Parameters.AddWithValue("@Gender", updatedPerson.GenderId);
                personCommand.Parameters.AddWithValue("@PersonId", updatedPerson.Id);
                personCommand.ExecuteNonQuery();

                // Update the Student table
                string updateStudentQuery = @"UPDATE Student
                                      SET RegistrationNo = @RegistrationNo
                                      WHERE Id = @PersonId";
                SqlCommand studentCommand = new SqlCommand(updateStudentQuery, connection);
                studentCommand.Parameters.AddWithValue("@RegistrationNo", updatedPerson.RegistrationNo);
                studentCommand.Parameters.AddWithValue("@PersonId", updatedPerson.Id);
                studentCommand.ExecuteNonQuery();

                MessageBox.Show("Data updated successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            // Ensure that a row is selected
            if (studentdatagridView.SelectedRows.Count > 0)
            {
                // Get the ID of the selected person
                int personId = Convert.ToInt32(studentdatagridView.SelectedRows[0].Cells["PersonID"].Value);

                // Delete the person record from the Person table
                DeleteStudent(personId);

                // Refresh DataGridView to reflect the changes
                PopulateDataGridView();

                // Clear the textboxes or controls after deletion
                clearStudentData();
            }
            else
            {
                MessageBox.Show("Please select a row to delete.");
            }
        }

        private void DeleteStudent(int personId)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                // Delete the corresponding record from the Student table
                string deleteStudentQuery = @"DELETE FROM Student WHERE Id = @PersonId";
                SqlCommand studentCommand = new SqlCommand(deleteStudentQuery, connection);
                studentCommand.Parameters.AddWithValue("@PersonId", personId);
                studentCommand.ExecuteNonQuery();

                // Delete the person record from the Person table
                string deletePersonQuery = @"DELETE FROM Person WHERE Id = @PersonId";
                SqlCommand personCommand = new SqlCommand(deletePersonQuery, connection);
                personCommand.Parameters.AddWithValue("@PersonId", personId);
                personCommand.ExecuteNonQuery();

                MessageBox.Show("Record deleted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            PersonModel oPersonModel = new PersonModel();
            oPersonModel.FirstName = txt_firstname.Text;
            oPersonModel.LastName = txt_lastname.Text;
            oPersonModel.Contact = txt_contact.Text;
            oPersonModel.Email = txt_email.Text;
            oPersonModel.DateofBirth = dateofbirth.Value;
            if(genderComboBox.SelectedValue != null)
            {
                oPersonModel.GenderId = Convert.ToInt32(genderComboBox.SelectedValue);
            }
            oPersonModel.RegistrationNo = txt_RegistrationNo.Text;
            // Insert the data into the database
            InsertStudentData(oPersonModel);

            PopulateDataGridView();
            clearStudentData();
        }

        private void InsertStudentData(PersonModel oPersonModel)
        {
            try
            {
                int personId = -1;
                var connection = Configuration.getInstance().getConnection();

                string personquery = @"INSERT INTO Person (FirstName, LastName, Email, Contact, DateOfBirth, Gender) 
                    VALUES (@FirstName, @LastName, @Email, @Contact, @DateOfBirth, @Gender);
                    SELECT SCOPE_IDENTITY();";
                SqlCommand command = new SqlCommand(personquery, connection);
                command.Parameters.AddWithValue("@FirstName", oPersonModel.FirstName);
                command.Parameters.AddWithValue("@LastName", oPersonModel.LastName);
                command.Parameters.AddWithValue("@Contact", oPersonModel.Contact);
                command.Parameters.AddWithValue("@Email", oPersonModel.Email);
                command.Parameters.AddWithValue("@Gender", oPersonModel.GenderId);
                command.Parameters.AddWithValue("@DateOfBirth", oPersonModel.DateofBirth);
                // Retrieve the inserted personId
                personId = Convert.ToInt32(command.ExecuteScalar());
                string studentquery = @"INSERT INTO Student(Id, RegistrationNo)
                    VALUES (@PersonId, @RegistrationNo)";
                SqlCommand studentCommand = new SqlCommand(studentquery, connection);
                studentCommand.Parameters.AddWithValue("@PersonId", personId);
                studentCommand.Parameters.AddWithValue("@RegistrationNo", oPersonModel.RegistrationNo);
                studentCommand.ExecuteNonQuery();

                MessageBox.Show("Data inserted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

          
        }


        private void clearStudentData()
        {
            txt_firstname.Text = "";
            txt_lastname.Text = "";
            txt_contact.Text = "";
            txt_email.Text = "";
            dateofbirth.Value = DateTime.Now; // Set to default date or any desired default value
            genderComboBox.SelectedIndex = 0; // Set to default or clear the selected index
            txt_RegistrationNo.Text = "";
            this.selectedPersonID = 0;
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Hide();
        }

        private void btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
