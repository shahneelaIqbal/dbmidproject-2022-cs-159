﻿using FinalYearProjectManagementApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalYearProjectManagementApp
{
    public partial class ManageStudentGroup : Form
    {
        private int selectedGroupID = 0;
        public ManageStudentGroup()
        {
            InitializeComponent();
            PopulateStatusComboBox();
            PopulateStudentsList(); // Populate list of available students

            PopulateDataGridView();

            studentgroupdatagridView.CellClick += studentgroupdatagridView_CellClick;

            lstStudent.ItemSelectionChanged += lstStudent_ItemSelectionChanged;
        }

        private void PopulateStatusComboBox()
        {
            statusComboBox.DataSource = GetStatus().ToList(); // Bind directly to the dictionary
            statusComboBox.DisplayMember = "Value"; // Display the designation string
            statusComboBox.ValueMember = "Key"; // Use the ID as the value

            // Set default selected value
            if (statusComboBox.Items.Count > 0)
            {
                statusComboBox.SelectedIndex = 0;
            }
        }

        private Dictionary<int, string> GetStatus()
        {
            try
            {
                Dictionary<int, string> status = new Dictionary<int, string>();
                string query = @"SELECT Id, Value AS Status FROM LookUp WHERE Category = 'STATUS'";

                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);

                int default_id = Convert.ToInt32(0);
                string default_designation = "Select status";
                status.Add(default_id, default_designation);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader["Id"]);
                        string desg = reader["Status"].ToString();
                        status.Add(id, desg);
                    }
                }
                return status;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return null;
            }
        }

        private void PopulateStudentsList()
        {
            string query = @"SELECT P.Id AS PersonID, P.FirstName + ' ' + p.LastName + ' (RegNo: ' + S.RegistrationNo + ')' AS Name
                FROM Person P 
                INNER JOIN Student S ON P.Id = S.Id";

            var connection = Configuration.getInstance().getConnection();

            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();

            // Clear existing items in the ListView
            lstStudent.Items.Clear();

            // Iterate through the results and add each student to the ListView
            while (reader.Read())
            {
                // Create a new ListViewItem
                ListViewItem item = new ListViewItem(reader["Name"].ToString());

                // Set the Tag property to store the student ID
                item.Tag = reader["PersonID"];

                // Add the ListViewItem to the ListView
                lstStudent.Items.Add(item);
            }
            reader.Close();
        }

        private void PopulateDataGridView()
        {
            string query = @"SELECT G.Id AS GroupID, GS.StudentId AS StudentID, 
                    P.FirstName + ' ' + P.LastName + ' (RegNo: ' + S.RegistrationNo + ')' AS StudentName, 
                    L.Value AS Status, GS.Status AS StatusID, GS.AssignmentDate 
                    FROM GroupStudent GS
                    INNER JOIN [Group] G ON G.Id = GS.GroupId
                    INNER JOIN Student S ON GS.StudentId = S.Id
                    INNER JOIN Person P ON S.Id = P.Id
                    LEFT JOIN [Lookup] L ON GS.Status = L.Id AND L.Category = 'STATUS'";
            try
            {
                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "StudentGroup");
                studentgroupdatagridView.DataSource = dataSet.Tables["StudentGroup"];
                studentgroupdatagridView.AutoResizeColumns();
                studentgroupdatagridView.Columns["StudentID"].Visible = false;
                studentgroupdatagridView.Columns["StatusID"].Visible = false;
                studentgroupdatagridView.Columns["AssignmentDate"].DefaultCellStyle.Format = "yyyy-MM-dd";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        
        private void btn_Save_Click(object sender, EventArgs e)
        {
            List<GroupStudentModel> groupStudentModels = new List<GroupStudentModel>(); 
            // List to store selected student IDs
            List<int> selectedStudentIds = new List<int>();

            // Iterate through selected items in lstStudent and add their IDs to the list
            foreach (ListViewItem item in lstStudent.SelectedItems)
            {
                int studentId = (int)item.Tag;
                selectedStudentIds.Add(studentId);
            }

            // Create GroupStudentModel objects for selected students and add them to the groupStudentModels list
            foreach (int studentId in selectedStudentIds)
            {
                GroupStudentModel groupStudentModel = new GroupStudentModel();
                groupStudentModel.StudentID = studentId;
                if(statusComboBox.SelectedValue != null)
                {
                    groupStudentModel.StatusID = Convert.ToInt32(statusComboBox.SelectedValue);
                }
                groupStudentModel.AssignmentDate = dateTimePicker1.Value;


                // Add the GroupStudentModel to the list
                groupStudentModels.Add(groupStudentModel);
            }

            InsertGroupStudentData(groupStudentModels);
            // Refresh DataGridView to reflect the changes
            PopulateDataGridView();

            // Clear the textboxes or controls after deletion
            ClearStudentGroupData();
        }

        private void InsertGroupStudentData(List<GroupStudentModel> groupStudentModels)
        {
            var connection = Configuration.getInstance().getConnection();
            int groupId = -1;
            // Insert the data into the Advisor table with the next available ID
            string insertQuery = @"INSERT INTO [Group] (Created_On) 
                                VALUES (GETDATE());
                                SELECT SCOPE_IDENTITY();";
            SqlCommand command = new SqlCommand(insertQuery, connection);
            groupId = Convert.ToInt32(command.ExecuteScalar());

            foreach (var item in groupStudentModels)
            {
                string studentquery = @"INSERT INTO GroupStudent(GroupId, StudentId, Status, AssignmentDate)
                    VALUES (@GroupID, @StudentID, @Status, @AssignmentDate)";
                SqlCommand studentCommand = new SqlCommand(studentquery, connection);
                studentCommand.Parameters.AddWithValue("@StudentID", item.StudentID);
                studentCommand.Parameters.AddWithValue("@Status", item.StatusID);
                studentCommand.Parameters.AddWithValue("@GroupID", groupId);
                studentCommand.Parameters.AddWithValue("@AssignmentDate", item.AssignmentDate);
                studentCommand.ExecuteNonQuery();
            }
            MessageBox.Show("Data inserted successfully.");
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Hide();
        }

        private void studentgroupdatagridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                // Check if the clicked cell is in the "GroupID" column
                if (studentgroupdatagridView.Columns[e.ColumnIndex].Name == "GroupID")
                {
                    DataGridViewRow row = studentgroupdatagridView.Rows[e.RowIndex];
                    int groupID = Convert.ToInt32(row.Cells["GroupID"].Value);
                    dateTimePicker1.Value = Convert.ToDateTime(row.Cells["AssignmentDate"].Value);
                    statusComboBox.SelectedValue = Convert.ToInt32(row.Cells["StatusID"].Value);
                    // Populate lstStudent with students in the selected group

                    selectedGroupID = groupID;
                    PopulateStudentsInGroupListView(groupID);
                }
            }
        }

        private void lstStudent_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            // Check if the item is selected
            if (e.IsSelected)
            {
                // Make the selected item's text bold
                e.Item.Font = new Font(e.Item.Font, FontStyle.Bold);
            }
            else
            {
                // If deselected, revert to regular font style
                e.Item.Font = new Font(e.Item.Font, FontStyle.Regular);
            }
        }
        
        private void PopulateStudentsInGroupListView(int groupID)
        {
            string query = @"SELECT P.FirstName + ' ' + P.LastName + ' (RegNo: ' + S.RegistrationNo + ')' AS StudentName, S.Id AS StudentID
                    FROM GroupStudent GS
                    INNER JOIN Student S ON GS.StudentId = S.Id
                    INNER JOIN Person P ON S.Id = P.Id
                    WHERE GS.GroupId = @GroupID";

            try
            {
                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@GroupID", groupID);

                SqlDataReader reader = command.ExecuteReader();

                // Get selected student IDs for comparison
                List<int> selectedStudentIds = new List<int>();
                foreach (ListViewItem item in lstStudent.SelectedItems)
                {
                    selectedStudentIds.Add((int)item.Tag);
                }

                // Create a dictionary to store ListViewItem references by student ID
                Dictionary<int, ListViewItem> studentItems = new Dictionary<int, ListViewItem>();
                foreach (ListViewItem item in lstStudent.Items)
                {
                    int studentID = Convert.ToInt32(item.Tag);
                    studentItems.Add(studentID, item);
                }

                // Iterate through the results and update existing list items or add new ones
                while (reader.Read())
                {
                    int studentID = Convert.ToInt32(reader["StudentID"]);
                    string studentName = reader["StudentName"].ToString();

                    // Check if the student is already in the list
                    if (studentItems.ContainsKey(studentID))
                    {
                        // Update existing item
                        ListViewItem item = studentItems[studentID];
                        item.Text = studentName;

                        // Mark the item as selected if it was previously selected
                        if (selectedStudentIds.Contains(studentID))
                        {
                            item.Selected = true;
                        }
                    }
                    else
                    {
                        // Add new item
                        ListViewItem item = new ListViewItem(studentName);
                        item.Tag = studentID;

                        // Mark the item as selected if it was previously selected
                        if (selectedStudentIds.Contains(studentID))
                        {
                            item.Selected = true;
                        }

                        lstStudent.Items.Add(item);
                    }
                }

                // Remove items that are no longer in the group
                foreach (ListViewItem item in lstStudent.Items.Cast<ListViewItem>().ToList())
                {
                    int studentID = Convert.ToInt32(item.Tag);
                    if (!studentItems.ContainsKey(studentID))
                    {
                        lstStudent.Items.Remove(item);
                    }
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            if(selectedGroupID != 0)
            {
                List<GroupStudentModel> groupStudentModels = new List<GroupStudentModel>();
                List<int> selectedStudentIds = new List<int>();

                // Iterate through selected items in lstStudent and add their IDs to the list
                foreach (ListViewItem item in lstStudent.SelectedItems)
                {
                    int studentId = (int)item.Tag;
                    selectedStudentIds.Add(studentId);
                }

                // Create GroupStudentModel objects for selected students and add them to the groupStudentModels list
                foreach (int studentId in selectedStudentIds)
                {
                    GroupStudentModel groupStudentModel = new GroupStudentModel();
                    groupStudentModel.StudentID = studentId;
                    if (statusComboBox.SelectedValue != null)
                    {
                        groupStudentModel.StatusID = Convert.ToInt32(statusComboBox.SelectedValue);
                    }
                    groupStudentModel.AssignmentDate = dateTimePicker1.Value;


                    // Add the GroupStudentModel to the list
                    groupStudentModels.Add(groupStudentModel);
                }

                UpdateStudentGroupData(groupStudentModels);

                // Refresh DataGridView to reflect the changes
                PopulateDataGridView();

                // Clear the textboxes or controls after deletion
                ClearStudentGroupData();
            }
            else
            {
                MessageBox.Show("Please select group");
            }
            
        }

        private void UpdateStudentGroupData(List<GroupStudentModel> groupStudentModels)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                foreach (var item in groupStudentModels)
                {
                    string updateQuery = @" DELETE FROM GroupStudent WHERE GroupId = @GroupId
                        IF EXISTS (SELECT * FROM GroupStudent WHERE GroupId = @GroupId AND StudentId = @StudentId)
                        BEGIN
	                        UPDATE GroupStudent 
	                        SET Status = @Status, AssignmentDate = @AssignementDate
                             WHERE GroupId = @GroupId AND StudentId = @StudentId
                        END
                        ELSE
                        BEGIN
	                        INSERT INTO GroupStudent(GroupId, StudentId, Status, AssignmentDate)
                            VALUES (@GroupID, @StudentID, @Status, @AssignementDate)
                        END";

                    SqlCommand personCommand = new SqlCommand(updateQuery, connection);
                    personCommand.Parameters.AddWithValue("@Status", item.StatusID);
                    personCommand.Parameters.AddWithValue("@AssignementDate", item.AssignmentDate);
                    personCommand.Parameters.AddWithValue("@GroupId", selectedGroupID);
                    personCommand.Parameters.AddWithValue("@StudentId", item.StudentID);
                    personCommand.ExecuteNonQuery();
                }
                
                MessageBox.Show("Data updated successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            // Ensure that a row is selected
            if (studentgroupdatagridView.SelectedRows.Count > 0)
            {
                // Get the ID of the selected person
                int groupId = Convert.ToInt32(studentgroupdatagridView.SelectedRows[0].Cells["GroupID"].Value);

                // Delete the person record from the Person table
                DeleteStudentGroup(groupId);

                // Refresh DataGridView to reflect the changes
                PopulateDataGridView();

                // Clear the textboxes or controls after deletion
                ClearStudentGroupData();
            }
            else
            {
                MessageBox.Show("Please select a group to delete.");
            }
        }

        private void DeleteStudentGroup(int groupId)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                string deleteStudentQuery = @"DELETE FROM GroupStudent WHERE GroupId = @GroupId";
                SqlCommand studentCommand = new SqlCommand(deleteStudentQuery, connection);
                studentCommand.Parameters.AddWithValue("@GroupId", groupId);
                studentCommand.ExecuteNonQuery();

                string deletePersonQuery = @"DELETE FROM [Group] WHERE Id = @GroupId";
                SqlCommand personCommand = new SqlCommand(deletePersonQuery, connection);
                personCommand.Parameters.AddWithValue("@GroupId", groupId);
                personCommand.ExecuteNonQuery();

                MessageBox.Show("Group deleted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    
        private void ClearStudentGroupData()
        {
            dateTimePicker1.Value = DateTime.Now;
            statusComboBox.SelectedIndex = 0; 
            this.selectedGroupID = 0;
        }

        private void btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
