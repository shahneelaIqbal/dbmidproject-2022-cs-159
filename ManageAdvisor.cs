﻿using FinalYearProjectManagementApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalYearProjectManagementApp
{
    public partial class ManageAdvisor : Form
    {
        private Dictionary<int, string> designation = new Dictionary<int, string>();
        private int selectedAdvisorID = 0;
        public ManageAdvisor()
        {
            InitializeComponent();
            PopulateDesignationComboBox();
            PopulateDataGridView();

            advisordatagridView.CellClick += advisordatagridView_CellClick;
        }

        private void PopulateDataGridView()
        {
            // SQL query to select data from the Students table
            string query = @"SELECT A.Id AS AdvisorID, A.Designation AS DesignationID, L.Value AS Designation, A.Salary AS Salary
                FROM Advisor A
                LEFT JOIN Lookup L ON A.Designation = L.Id 
                WHERE L.Category = 'DESIGNATION'";

            try
            {
                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Advisors");
                advisordatagridView.DataSource = dataSet.Tables["Advisors"];
                advisordatagridView.AutoResizeColumns();
                advisordatagridView.Columns["AdvisorID"].Visible = false;
                advisordatagridView.Columns["DesignationID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void PopulateDesignationComboBox()
        {
            designationComboBox.DataSource = GetDesignation().ToList(); // Bind directly to the dictionary
            designationComboBox.DisplayMember = "Value"; // Display the designation string
            designationComboBox.ValueMember = "Key"; // Use the ID as the value

            // Set default selected value
            if (designationComboBox.Items.Count > 0)
            {
                designationComboBox.SelectedIndex = 0;
            }
        }

        private Dictionary<int, string> GetDesignation()
        {
            try
            {
                Dictionary<int, string> designations = new Dictionary<int, string>();
                string query = @"SELECT Id, Value AS Designation FROM LookUp WHERE Category = 'DESIGNATION'";

                var connection = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand(query, connection);

                int default_id = Convert.ToInt32(0);
                string default_designation = "Select designation";
                designations.Add(default_id, default_designation);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader["Id"]);
                        string desg = reader["Designation"].ToString();
                        designations.Add(id, desg);
                    }
                }
                return designations;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return null;
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            // Insert the data into the database
            InsertAdvisortData(txt_salary.Text, Convert.ToInt32(designationComboBox.SelectedValue));

            PopulateDataGridView();
            ClearAdvisorData();
        }

        private void InsertAdvisortData(string salary, int designationID)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                // Get the maximum ID from the Advisor table
                int maxId = 0;
                string getMaxIdQuery = "SELECT MAX(Id) FROM Advisor";
                SqlCommand getMaxIdCommand = new SqlCommand(getMaxIdQuery, connection);
                object maxIdResult = getMaxIdCommand.ExecuteScalar();
                if (maxIdResult != DBNull.Value)
                {
                    maxId = Convert.ToInt32(maxIdResult);
                }

                // Increment the maxId to get the next available ID
                int nextId = maxId + 1;

                // Insert the data into the Advisor table with the next available ID
                string insertQuery = @"INSERT INTO Advisor (Id, Salary, Designation) 
                                VALUES (@Id, @Salary, @DesignationID);";
                SqlCommand command = new SqlCommand(insertQuery, connection);
                command.Parameters.AddWithValue("@Id", nextId);
                command.Parameters.AddWithValue("@Salary", Convert.ToDecimal(salary));
                command.Parameters.AddWithValue("@DesignationID", designationID);
                command.ExecuteNonQuery();

                MessageBox.Show("Data inserted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void advisordatagridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) // Check if a valid row is clicked
            {
                DataGridViewRow row = advisordatagridView.Rows[e.RowIndex];

                // Populate textboxes or controls with data from the selected row
                txt_salary.Text = row.Cells["Salary"].Value.ToString();
                designationComboBox.SelectedValue = Convert.ToInt32(row.Cells["DesignationID"].Value);

                // Retrieve and store the PersonID from the clicked row
                selectedAdvisorID = Convert.ToInt32(row.Cells["AdvisorID"].Value);
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            // Pass the updated data to the update method
            UpdateAdvisorData(selectedAdvisorID, txt_salary.Text, Convert.ToInt32(designationComboBox.SelectedValue));

            PopulateDataGridView();
            ClearAdvisorData();
        }

        private void UpdateAdvisorData(int selectedAdvisorID, string salary, int designationID)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                string updateQuery = @"UPDATE Advisor
                               SET Salary = @Salary, Designation = @Designation
                               WHERE Id = @AdvisorId";

                SqlCommand advisorCommand = new SqlCommand(updateQuery, connection);
                advisorCommand.Parameters.AddWithValue("@Salary", salary);
                advisorCommand.Parameters.AddWithValue("@Designation", designationID);
                advisorCommand.Parameters.AddWithValue("@AdvisorId", selectedAdvisorID);
                advisorCommand.ExecuteNonQuery();

                MessageBox.Show("Data updated successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            // Ensure that a row is selected
            if (advisordatagridView.SelectedRows.Count > 0)
            {
                // Get the ID of the selected person
                int advisorId = Convert.ToInt32(advisordatagridView.SelectedRows[0].Cells["AdvisorID"].Value);

                // Delete the person record from the Person table
                DeletePerson(advisorId);

                // Refresh DataGridView to reflect the changes
                PopulateDataGridView();

                // Clear the textboxes or controls after deletion
                ClearAdvisorData();
            }
            else
            {
                MessageBox.Show("Please select a row to delete.");
            }
        }

        private void DeletePerson(int advisorId)
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();

                // Delete the corresponding record from the Student table
                string deleteQuery = @"DELETE FROM Advisor WHERE Id = @AdvisorID";
                SqlCommand studentCommand = new SqlCommand(deleteQuery, connection);
                studentCommand.Parameters.AddWithValue("@AdvisorID", advisorId);
                studentCommand.ExecuteNonQuery();

                MessageBox.Show("Record deleted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void ClearAdvisorData()
        {
            txt_salary.Text = "";
            designationComboBox.SelectedIndex = 0; 
            this.selectedAdvisorID = 0;
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Hide();
        }

        private void btn_Exist_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
