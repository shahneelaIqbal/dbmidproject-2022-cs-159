﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalYearProjectManagementApp
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void btn_manageStudent_Click(object sender, EventArgs e)
        {
            ManageStudents manageStudentForm = new ManageStudents();
            manageStudentForm.Show();
            this.Hide();
        }

        private void btn_manageProject_Click(object sender, EventArgs e)
        {
            ManageProjects manageProjectsForm = new ManageProjects();
            manageProjectsForm.Show();
        }

        private void btn_manageAdvisor_Click(object sender, EventArgs e)
        {
            ManageAdvisor manageAdvisorForm = new ManageAdvisor();
            manageAdvisorForm.Show();
            this.Hide();
        }

        private void btn_manageStudentGroup_Click(object sender, EventArgs e)
        {
            ManageStudentGroup manageStudentGroup = new ManageStudentGroup();
            manageStudentGroup.Show();
            this.Hide();
        }

        private void btn_manageProjectAdvisor_Click(object sender, EventArgs e)
        {
            ManageProjectAdvisor manageProjectAdvisor = new ManageProjectAdvisor();
            manageProjectAdvisor.Show();
            this.Hide();
        }
    }
}
