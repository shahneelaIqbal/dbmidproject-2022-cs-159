﻿namespace FinalYearProjectManagementApp
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btn_manageStudent = new System.Windows.Forms.Button();
            this.btn_manageAdvisor = new System.Windows.Forms.Button();
            this.btn_manageProject = new System.Windows.Forms.Button();
            this.btn_manageStudentGroup = new System.Windows.Forms.Button();
            this.btn_manageStudentProjects = new System.Windows.Forms.Button();
            this.btn_manageProjectAdvisor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(280, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Final Year Project Managment";
            // 
            // btn_manageStudent
            // 
            this.btn_manageStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_manageStudent.Location = new System.Drawing.Point(63, 137);
            this.btn_manageStudent.Name = "btn_manageStudent";
            this.btn_manageStudent.Size = new System.Drawing.Size(141, 43);
            this.btn_manageStudent.TabIndex = 1;
            this.btn_manageStudent.Text = "Manage students";
            this.btn_manageStudent.UseVisualStyleBackColor = true;
            this.btn_manageStudent.Click += new System.EventHandler(this.btn_manageStudent_Click);
            // 
            // btn_manageAdvisor
            // 
            this.btn_manageAdvisor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_manageAdvisor.Location = new System.Drawing.Point(284, 137);
            this.btn_manageAdvisor.Name = "btn_manageAdvisor";
            this.btn_manageAdvisor.Size = new System.Drawing.Size(141, 43);
            this.btn_manageAdvisor.TabIndex = 2;
            this.btn_manageAdvisor.Text = "Manage advisor";
            this.btn_manageAdvisor.UseCompatibleTextRendering = true;
            this.btn_manageAdvisor.UseVisualStyleBackColor = true;
            this.btn_manageAdvisor.Click += new System.EventHandler(this.btn_manageAdvisor_Click);
            // 
            // btn_manageProject
            // 
            this.btn_manageProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_manageProject.Location = new System.Drawing.Point(512, 137);
            this.btn_manageProject.Name = "btn_manageProject";
            this.btn_manageProject.Size = new System.Drawing.Size(141, 43);
            this.btn_manageProject.TabIndex = 3;
            this.btn_manageProject.Text = "Manage projects";
            this.btn_manageProject.UseVisualStyleBackColor = true;
            this.btn_manageProject.Click += new System.EventHandler(this.btn_manageProject_Click);
            // 
            // btn_manageStudentGroup
            // 
            this.btn_manageStudentGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_manageStudentGroup.Location = new System.Drawing.Point(284, 254);
            this.btn_manageStudentGroup.Name = "btn_manageStudentGroup";
            this.btn_manageStudentGroup.Size = new System.Drawing.Size(141, 55);
            this.btn_manageStudentGroup.TabIndex = 4;
            this.btn_manageStudentGroup.Text = "Manage students group";
            this.btn_manageStudentGroup.UseVisualStyleBackColor = true;
            this.btn_manageStudentGroup.Click += new System.EventHandler(this.btn_manageStudentGroup_Click);
            // 
            // btn_manageStudentProjects
            // 
            this.btn_manageStudentProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_manageStudentProjects.Location = new System.Drawing.Point(512, 254);
            this.btn_manageStudentProjects.Name = "btn_manageStudentProjects";
            this.btn_manageStudentProjects.Size = new System.Drawing.Size(141, 55);
            this.btn_manageStudentProjects.TabIndex = 5;
            this.btn_manageStudentProjects.Text = "Manage student projects";
            this.btn_manageStudentProjects.UseVisualStyleBackColor = true;
            // 
            // btn_manageProjectAdvisor
            // 
            this.btn_manageProjectAdvisor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_manageProjectAdvisor.Location = new System.Drawing.Point(63, 254);
            this.btn_manageProjectAdvisor.Name = "btn_manageProjectAdvisor";
            this.btn_manageProjectAdvisor.Size = new System.Drawing.Size(141, 55);
            this.btn_manageProjectAdvisor.TabIndex = 6;
            this.btn_manageProjectAdvisor.Text = "Manage project advisor";
            this.btn_manageProjectAdvisor.UseVisualStyleBackColor = true;
            this.btn_manageProjectAdvisor.Click += new System.EventHandler(this.btn_manageProjectAdvisor_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 553);
            this.Controls.Add(this.btn_manageProjectAdvisor);
            this.Controls.Add(this.btn_manageStudentProjects);
            this.Controls.Add(this.btn_manageStudentGroup);
            this.Controls.Add(this.btn_manageProject);
            this.Controls.Add(this.btn_manageAdvisor);
            this.Controls.Add(this.btn_manageStudent);
            this.Controls.Add(this.label1);
            this.Name = "Home";
            this.Text = "Home";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_manageStudent;
        private System.Windows.Forms.Button btn_manageAdvisor;
        private System.Windows.Forms.Button btn_manageProject;
        private System.Windows.Forms.Button btn_manageStudentGroup;
        private System.Windows.Forms.Button btn_manageStudentProjects;
        private System.Windows.Forms.Button btn_manageProjectAdvisor;
    }
}

