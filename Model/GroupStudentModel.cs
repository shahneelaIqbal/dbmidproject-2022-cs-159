﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalYearProjectManagementApp.Model
{
    internal class GroupStudentModel
    {
        public int StudentID { get; set; }
        public int StatusID { get; set; }
        public DateTime AssignmentDate { get; set; }
    }
}
