﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalYearProjectManagementApp.Model
{
    internal class PersonModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public DateTime DateofBirth { get; set; }
        public int GenderId { get; set; }
        public string RegistrationNo { get; set; }
    }
}
